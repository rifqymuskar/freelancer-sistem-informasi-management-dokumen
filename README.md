# Aplikasi Monitoring Korban & Tagihan RS
Aplikasi ini berguna untuk membantu dari segi management Monitoring data Korban dan Tagihan Rumas Sakit

## Getting Started
### Teknologi
* Aplikasi ini menggunakan Framework CodeIgniter sebagai sisi Server 
* Aplikasi ini menggunakan Framework AngularJS sebagai sisi Client

### Instalasi
1. Clone Repository pada Branch Master.
```sh
git clone https://gitlab.com/rifqymuskar/freelancer-sistem-informasi-management-dokumen.git
```

2. Extrak file ke dalam Web Server anda.
3. edit konfigurasi database.
```sh
application/config/database.php
```
4. sesuaikan dengan database yang anda punya.
5. jalankan Aplikasi melalui browser anda.
6. pastikan Cookies pada browser anda aktif.

