<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");

class Home extends CI_Controller {

    public function __construct(){
        parent::__construct();
        // ...........................
        $this->load->helper('login_helper');

        $this->domain = 'iwan.rifqymuskar.com';

        if($this->input->cookie('idCookie')){
            $check = $this->db->where('id', $this->input->cookie('idCookie'))->get('cookies_login');
                if($check->num_rows() > 0){
                    if($check->row()->status != 0){
                        redirect("");  
                    }
                }else{
                    foreach($this->input->cookie() as $key => $value){
                        delete_cookie($key, $this->domain);
                    }
                }
        }else{
            redirect("");  
        }

        if($this->input->cookie('role') != "1"){
            redirect("");  
        }
    }

    public function index(){
        $data['app_version'] = $this->db->get("app_version")->row();
        $data['role'] = "admin";
        $this->load->view('admin/index', $data);
    }
    public function route($name=''){
        $this->load->view('admin/'.$name);
    }
}