<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");

class Response extends CI_Controller {
    public function app_version(){

        date_default_timezone_set("Asia/Makassar");

        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');
  
        $time = $this->db->get("app_version")->row()->version;
        echo "data: {$time}\n\n";
        flush();
    }
    public function forgotPassword($token){
        $this->load->library('encryption');
        echo $this->encryption->decrypt($token);
    }
}