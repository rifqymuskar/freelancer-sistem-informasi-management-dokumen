<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");

class Auth extends CI_Controller {

    public function __construct(){
        parent::__construct();
        // ...........................
        $this->load->helper('login_helper');

        $this->domain = urlSekarang();

        if($this->input->cookie('idCookie')){

          $check = $this->db->where('id', $this->input->cookie('idCookie'))->get('cookies_login');

          if($check->num_rows() > 0){

            if($check->row()->status == 0){

              switch ($this->input->cookie('role')) {
                case "1":
                  redirect("admin");
                  break;
                case "2":
                  redirect("uploader");
                  break;
                case "3":
                  redirect("viewer");
                  break;
                // case "2":
                //     redirect("general");
                //   break;
              }
            }else{
              foreach($this->input->cookie() as $key => $value){
                delete_cookie($key, $this->domain);
              }
            }
          }else{
            foreach($this->input->cookie() as $key => $value){
              delete_cookie($key, $this->domain);
            }
          }
        }

    }

    public function index(){
        $data['app_version'] = $this->db->get("app_version")->row();
        $this->load->view('auth/index', $data);
    }
    public function route($name=''){
        $data['cookies'] = $this->input->cookie();
        $this->load->view('auth/'.$name, $data);
    }

    // public function failed_auth(){
    //     $this->status('failed', 'gagal, auth gagal', '');
    // }
}