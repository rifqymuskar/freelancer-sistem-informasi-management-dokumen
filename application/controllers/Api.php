<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");


use chriskacerguis\RestServer\RestController;

class Api extends RestController {

    function __construct() {
        parent::__construct();
        $this->load->helper('login_helper');
        $db_debug = $this->db->db_debug; //save setting
        $this->db->db_debug = FALSE; //disable debugging for queries

        $this->domain = urlSekarang();
        date_default_timezone_set("Asia/Makassar");
    }

    // DEFAULT FUNCTION =========================================================
    public function index_get($table=''){

        $this->db->trans_begin();
        $id = $this->get('id');

        // if($this->db->field_exists('status', $table)){
        //     $this->db->where("status", 1);
        // }

        if ($id == '') {
            $id = '';

            $this->db->order_by("create_at", "DESC");
            $result = $this->db->get($table)->result();

        } else {

            $this->db->where('id', $id);
            $result = $this->db->get($table)->result();

        }
        if($result){

            if($table == 'document'){
                foreach($result as $key => $value){
                    $result[$key]->description = $result[$key]->description;
                    $result[$key]->descriptionFull = strip_tags($result[$key]->description);
                }
            }

            $this->checkTransaction('success get data from '.$table, $id, $result);

        }else{
            if($this->db->error()['message']){
                $message = $this->db->error()['message'];
            }else{
                $message = 'data not found';
            }
            $this->response(array('status' => 'failed', 'message' => $message), 200);
            $this->db->trans_rollback();
        }
    }
    public function getDataByKey_get($table=''){
        $data = $this->get();
        $result = $this->db->where($data['key'], $data['value'])->get($table)->result();
        if($result){
            $this->checkTransaction('success get data from custom data', '', $result);
        }else{
            if($this->db->error()['message']){
                $message = $this->db->error()['message'];
            }else{
                $message = 'data not found';
            }
            $this->response(array('status' => 'failed', 'message' => $message), 200);
            $this->db->trans_rollback();
        }
    }
    public function index_post($table='') {

        $this->db->trans_begin();

        $data = $this->post(); 
        
        $data['user_id'] = $this->session->userdata('id');

        if(isset($data['password'])){
            $data['password'] = md5($data['password']);
        }

        $result = $this->db->insert($table, $data);
        if($result){
            $this->checkTransaction('success insert data into '.$table, $data['id'], $data);
        }else{
            $this->response(array('status' => 'error', 'message' => $this->db->error()['message']), 200);
            $this->db->trans_rollback();
        }
    }
    public function index_put($table='') {

        $this->db->trans_begin();
        $data = $this->put();
        $data['user_id'] = $this->session->userdata('id');

        if(count($data) == 1){
            $data = json_decode($this->put()[0], true);
        }

        if(isset($data['file'])){
            $file = $data['file'];
            unset($data['file']);
        }

        if(isset($data)){

            if(isset($data['id'])){
                $id = $data['id'];
                unset($data['id']);
    
                if(isset($data['file'])){
                    $file = $data['file'];
                    unset($data['file']);
                }

                if($table == 'document'){
                    unset($data['descriptionFull']);
                    $data['tahun'] = date('Y');
                }

                if(isset($data['password'])){
                    $data['password'] = md5($data['password']);
                }

                if(isset($data['token'])){
                    $token = $data['token'];
                    unset($data['token']);
                    $this->db->where("token", $token)->set("status", 1)->update("users_forgot");
                }

                $check = $this->db->where('id', $id)->get($table)->num_rows();
                if($check > 0){
                    $result = $this->db->where('id', $id)->update($table, $data);
                    if($result){

                        if(isset($file)){
                            if(count($file) > 0){
                                if($table == 'document'){
                                    $this->db->where_in('id', $file)->set('document_id', $id)->update('document_file');
                                }
                            }
                        }

                        $this->checkTransaction('Berhasil, Update Data di '.$table. ' pada ID '.$id, $id, $data);
                    }else{
                        $this->response(array('status' => 'failed', 'message' => $this->db->error()['message']), 200);
                        $this->db->trans_rollback();
                    }
                }else{
                    $this->response(array('status' => 'failed', 'message' => 'id not found in table '.$table), 200);
                    $this->db->trans_rollback(); 
                }
            }else{

                $data['id'] = $this->generate_id($table);

                if(isset($data['file'])){
                    $file = $data['file'];
                    unset($data['file']);
                }

                if(isset($data['password'])){
                    $data['password'] = md5($data['password']);
                }

                if($table == 'document'){
                    $data['tahun'] = date('Y');
                }
        
                $result = $this->db->insert($table, $data);
                if($result){

                    if(isset($file)){
                        if(count($file) > 0){
                            if($table == 'document'){
                                $this->db->where_in('id', $file)->set('document_id', $data['id'])->update('document_file');
                            }
                        }
                    }

                    $this->checkTransaction('Berhasil, Memasukkan data di '.$table, $data['id'], $data);
                }else{
                    $this->response(array('status' => 'failed', 'message' => $this->db->error()['message']), 200);
                    $this->db->trans_rollback();
                }
                // $this->response(array('status' => 'failed', 'message' => 'no id'), 200);
                // $this->db->trans_rollback();
            }
        }else{
            $this->response(array('status' => 'failed', 'message' => 'no data'), 200);
            $this->db->trans_rollback();
        }
    }
    public function index_delete($table='', $id='') {
        $this->db->trans_begin();
        $this->load->helper("file");

        if($table == 'document_file'){
            $file = $this->db->where('id', $id)->get('document_file')->row()->name;
            unlink('./public/uploads/document/'.$file);
        }

        if($id){
            $check = $this->db->where('id', $id)->get($table)->num_rows();

            if($check > 0){
                $result = $this->db->where('id', $id)->delete($table);
                if($result){
                    $this->checkTransaction('Berhasil, Menghapus Data di '.$table. ' pada ID '.$id, $id, '');
                }else{
                    $this->response(array('status' => 'failed', 'message' => $this->db->error()['message']), 200);
                    $this->db->trans_rollback();
                }
            }else{
                $this->response(array('status' => 'failed', 'message' => 'id not found in table '.$table), 200);
                $this->db->trans_rollback();
            }
        }else{
            $this->response(array('status' => 'failed', 'message' => 'id empty'), 200);
            $this->db->trans_rollback();
        }
    }
    public function session_get(){
        $check = $this->db->where('id', $this->input->cookie('idCookie'))->get('cookies_login');
        if($check->num_rows() > 0){
            if($check->row()->status == 0){
                echo json_encode($this->input->cookie());
            }else{
                foreach($this->input->cookie() as $key => $value){
                    delete_cookie($key, 'inventory.vitobes.com');
                }
            }
        }else{
            foreach($this->input->cookie() as $key => $value){
                delete_cookie($key, 'inventory.vitobes.com');
            }
        }
    }
    // DEFAULT FUNCTION =========================================================

    public function checkUser_post($table=''){
        $this->db->trans_begin();
        $data = $this->post();

        $this->db->where('username', $data['user']);
        $this->db->where('password', md5($data['password']));

        $check = $this->db->get('users');
        if($check->num_rows() > 0){
            $session = $check->row_array();

            $session['roleName'] = $this->db->where('id', $session['role'])->get('users_role')->row()->name;
            $session['positionName'] = $this->db->where('id', $session['position'])->get('users_position')->row()->name;
            $session['departmentName'] = $this->db->where('id', $session['department'])->get('users_department')->row()->name;
            $session['departmentKode'] = $this->db->where('id', $session['department'])->get('users_department')->row()->kode;
            $session['companyName'] = $this->db->where('id', $session['company'])->get('users_company')->row()->name;

            $this->session->set_userdata($session);

            delete_cookie('user_id');

            $idCookie = $this->generate_id('cookies_login');

            $dataCookie = array(
                'id' => $idCookie,
                'user_id' => $session['id'],
                'status' => 0
            );

            $check = $this->db->insert('cookies_login', $dataCookie);
       
            if($check){

                $this->input->set_cookie(array(
                    'name' => 'idCookie',
                    'value' => $idCookie,
                    'expire' => 86400*30,
                    'domain' => $this->domain,
                    'secure' => TRUE
                ));

                foreach($session as $key => $value){
                    $cookie = array(
                        'name' => $key,
                        'value' => $value,
                        'expire' => 86400*30,
                        'domain' => $this->domain,
                        'secure' => TRUE
                    );
                    $this->input->set_cookie($cookie);
                }
    
                $this->checkTransaction('Berhasil login', $session['id'], $session);

            }else{
                $this->response(array('status' => 'failed', 'message' => 'terjadi kesalahan oleh system, silahkan mencoba kembali.'), 200);
                $this->db->trans_rollback();
            }

        }else{
            $this->response(array('status' => 'failed', 'message' => 'Mohon maaf, User atau Password anda salah, apabila ini kesalahan, silahkan hubungi Admin. Terima kasih.'), 200);
            $this->db->trans_rollback();
        }

    }

    public function forgotPassword_post($table=''){
        $this->db->trans_begin();
        $this->load->library('encryption');
        $data = $this->post();

        $getUser = $this->db->where("email", $data['user'])->get("users");
        if($getUser->num_rows() > 0){
            $id = $this->generate_id("users_forgot");

            $ciphertext = encrypt_url($id);

            $dataUser = $getUser->row_array();
            $dataUser['url'] = base_url().'#!/newpassword/'.$ciphertext;

            $post = array(
                'id' => $id, 
                'user_id' => $dataUser['id'],
                'token' => $ciphertext
            );
    
            $send = $this->db->insert("users_forgot", $post);

            $this->sendMail($data['user'], "Forgot Password", "forgot_password", $dataUser);

        }else{
            $this->response(array('status' => 'failed', 'message' => 'Mohon maaf email yang anda masukkan tidak terdaftar di system kami.'), 200);
            $this->db->trans_rollback();
        }

    }

    public function checkToken_post(){
        $token = $this->post('token');
        $check = $this->db->where('token', $token)->where('status !=', 1)->get('users_forgot');

            if($check->num_rows() > 0){
                $this->response(array('status' => 'success', 'message' => 'token yang anda masukkan benar', 'data' => $check->row()), 200);
            }else{
                $this->response(array('status' => 'failed', 'message' => 'token yang anda masukkan salah atau kadaluwarsa. silahkan generate token baru.'), 200);
            }

    }

    public function logout_post(){
        $this->db->trans_begin();
        $data = $this->input->cookie();
        $this->db->where("id", $this->input->cookie('idCookie'))->set("status", 1)->update('cookies_login');
        foreach($this->input->cookie() as $key => $value){
            delete_cookie($key, $this->db);
        }
        // $data = $this->session->userdata;
        // $this->session->sess_destroy();
        $this->checkTransaction('berhasil logout', $data['id'], $data);
    }

    public function articleSend_post(){
        $data = $this->post();

        $data['id'] = $this->generate_id("article");

        $config['upload_path']          = './public/uploads/article/';
        $config['allowed_types']        = 'jpg|png|jpeg';
        $config['max_size']             = 2000;
        $config['max_width']            = 2000;
        $config['max_height']           = 1500;
        $config['overwrite']           = TRUE;
        $config['file_name']           = $data['id'];

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('thumbnail')){
            $this->response(array('status' => 'failed', 'message' => $this->upload->display_errors()), 200);
        }
        else{
            $dataImage = $this->upload->data();
            $data['thumbnail'] = $dataImage['file_name'];
            $data['date'] = date("Y-m-d");

            $data['title'] = preg_replace('/[^A-Za-z0-9\- ]/', '', $data['title']);

            $data['slug'] = str_replace(' ', '-', $data['title']);
            
            $data['slug'] = str_replace(',', '', $data['slug']);
            $data['slug'] = str_replace(':', '', $data['slug']);
            $data['slug'] = str_replace(';', '', $data['slug']);

            if(substr($data['slug'], -1) == '-'){
                $data['slug'] = substr($data['slug'], 0, -1);
            }

            $data['slug'] = strtolower($data['slug']);

            $data['url'] = 'https://suzukimotorsulawesi.co.id/article/'.$data['slug'];

            $result = $this->db->insert("article", $data);

            if($result){
                $this->checkTransaction('success insert data into article', $data['id'], $data);
            }else{
                $this->response(array('status' => 'error', 'message' => $this->db->error()['message']), 200);
                $this->db->trans_rollback();
            }
            
        }
    }

    public function getDivisi_get($table=''){
        $this->db->trans_begin();

        $result = $this->db->get($table)->result();

        foreach($result as $key => $value){
            $result[$key]->total_user = $this->db->where("department", $value->id)->get("users")->num_rows();
        }
        if($result){
            $this->checkTransaction('success get data from '.$table, '', $result);
        }else{
            if($this->db->error()['message']){
                $message = $this->db->error()['message'];
            }else{
                $message = 'data not found';
            }
            $this->response(array('status' => 'failed', 'message' => $message), 200);
            $this->db->trans_rollback();
        }
    }

    // UPLOADS =================================================================
    public function documentFile_post(){

        $data['id'] = $this->generate_id("document_file");

        $config['upload_path']          = './public/uploads/document/';
        $config['allowed_types']        = 'jpg|jpeg|png|pdf|doc|docx|xlsx|xls|csv';
        $config['max_size']             = 3000;
        $config['max_width']            = 4000;
        $config['max_height']           = 4000;
        $config['overwrite']           = TRUE;
        // $config['file_name']           = $data['id'];

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('photo')){
            $this->response(array('status' => 'failed', 'message' => $this->upload->display_errors()), 200);
        }else{
            $dataImage = $this->upload->data();
            $data['name'] = $dataImage['file_name'];
            $data['file_type'] = $dataImage['file_type'];
            $data['file_size'] = $dataImage['file_size'];
            $result = $this->db->insert("document_file", $data);
            if($result){
                $this->checkTransaction('success insert data into document', $data['id'], $data);
            }else{
                $this->response(array('status' => 'error', 'message' => $this->db->error()['message']), 200);
                $this->db->trans_rollback();
            }
        }
    }

    public function getDocumentId_get($table=''){
        $id = $this->generate_id_document();
        $this->checkTransaction('success insert data into document', $id, $id);
    }

    // =========================================================================
    private function checkTransaction($message='', $id='', $data=array()){
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->response(array('status' => 'error', 'message' => 'failed from transaction Codeigniter'), 200);
        }else{
            $this->db->trans_commit();
            $this->post_history($message, $id);
            $this->response(array('status' => 'success', 'message' => $message, 'data' => $data), 200);
        }		
    }
    private function checkTransactionWithoutHistory($message='', $result=array()){
        if ($this->db->trans_status() === FALSE){
            $this->response(array('status' => 'error', 'message' => 'failed from transaction Codeigniter'), 200);
            $this->db->trans_rollback();
        }else{
            $this->response(array('status' => 'success', 'message' => $message, 'data' => $result), 200);
            $this->db->trans_commit();
        }		
    }
    private function post_history($message='', $id=''){
        $agent['id'] = $this->generate_id('database_log');
        $agent['user_id'] = '1';
        $agent['activity'] = $message;
        $agent['table_id'] = $id;
        $agent['ip_address'] = $this->input->ip_address();
        $agent['platform'] = $this->agent->platform();
        $agent['browser'] = $this->agent->browser();
        $agent['browserVersion'] = $this->agent->version();
        $agent['is_mobile'] = $this->agent->mobile();
        $agent['is_robot'] = $this->agent->robot();
        $agent['referrer'] = $this->agent->referrer();
        $agent['agent_string'] = $this->agent->agent_string();
        $check = $this->db->insert('database_log', $agent);
    }
    private function generate_id($table=''){
        $initial = $this->db->where('table_name', $table)->get('tables_id');
        if($initial->num_rows() > 0){
            $initial = $initial->row()->id;
            $id = $this->db->query('SELECT generateId("'.$initial.date('ymds').'", "3") AS counter')->row()->counter;
        }else{
            $id = '';
            //$this->response(array('status' => 'failed', 'message' => 'failed, no data from table id_tables'), 200);
        }
        return $id;
    }
    private function generate_id_service($table=''){
        $id = $this->db->query('SELECT generateId("'.date('ym').'-", "3") AS counter')->row()->counter;
        return $id;
    }
    private function generate_id_document($table=''){
        $id = $this->db->query('SELECT generateId("'.date('Y').'/", "3") AS counter')->row()->counter;
        return $id;
    }

    private function sendMail($to='', $subject='', $file='', $data=array()){
        
        $this->load->library('email');

        $body = $this->load->view('emails/'.$file, $data, true);

        $result = $this->email
            ->from('rifqymuskar@gmail.com', 'Rifqy Muskar')
            ->reply_to('rifqymuskar@gmail.com')    // Optional, an account where a human being reads.
            ->to($to)
            ->subject($subject)
            ->message($body)
            ->send();

        if ($result) {
            $this->checkTransaction('success mengirim data', '', $result);
        } else {
            $this->response(array('status' => 'failed', 'message' => $this->email->print_debugger()), 200);
            $this->db->trans_rollback();
        }
        
    }

}