<section class="content-header">
    <h1>
        Data Kantor
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li class="active">Kantor</li>
    </ol>
</section>

<section class="content">

    <div class="box box-info box-solid">
        <div class="box-body no-padding">
            <table class="table table-hover" wt-responsive-table>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Kantor</th>
                        <th>Deskripsi</th>
                        <th>Alamat</th>
                        <th>Dibuat</th>
                        <th width="1%"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1.</td>
                        <td>Kantor Pusat</td>
                        <td>Tidak memiliki deskripsi</td>
                        <td>Jalan Raya Makassar Indah, No. 36c, Kota Makassar, Sulawesi Selatan</td>
                        <td>Senin, 03 juli 2021</td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-xs btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Pilihan
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                    <li><a>Edit</a></li>
                                    <li><a>Hapus</a></li>
                                    <!-- <li><a>Konfirmasi pembayaran</a></li> -->
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>Kantor Cabang 1</td>
                        <td>Tidak memiliki deskripsi</td>
                        <td>Jalan Andi Pangeran Pettarani No.59, Kota Makassar, Sulawesi Selatan</td>
                        <td>Selasa, 04 juli 2021</td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-xs btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Pilihan
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                    <li><a>Edit</a></li>
                                    <li><a>Hapus</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="text-right">
        <nav aria-label="...">
            <ul class="pagination" style="margin-top:0px;">
                <li class="disabled"><a href="" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
                <li class="active"><a href="">1 <span class="sr-only">(current)</span></a></li>
                <li><a href="">2</a></li>
                <li><a href="">3</a></li>
                <li><a href="">4</a></li>
                <li><a href="">5</a></li>
                <li><a href="" aria-label="Next"><span aria-hidden="true">»</span></a></li>
            </ul>
        </nav>
    </div>

</section>