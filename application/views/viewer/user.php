<section class="content-header">
    <h1>
        User
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li class="active">User</li>
    </ol>
</section>

<section class="content">

    <div class="row" style="margin-bottom:15px; margin-top:15px;">
        <div class="col-md-3" style="padding-right:5px;">
            <!-- <a href="#!user/new" class="btn btn-primary btn-flat">Tambah User</a> -->
        </div>
        <div class="col-md-3" style="padding-right:5px; padding-left:5px;">
            <select ng-change="changeKantor()" class="form-control" ng-model="search.company">
                <option value="">Pilih Kantor</option>
                <option value="{{x.id}}" ng-repeat="x in dataCabang">{{x.name}}</option>
            </select>
        </div>
        <div class="col-md-3" style="padding-left:5px; padding-right:5px;">
            <select ng-model="search.department" class="form-control">
                <option value="">Pilih Divisi</option>
                <option value="{{x.id}}" ng-repeat="x in dataDivisi" ng-if="x.company_id == search.company">{{x.name}}</option>
            </select>
        </div>
        <div class="col-md-3" style="padding-left:5px;">
            <div class="input-group">
                <input type="text" class="form-control" value="" ng-model="search.$" placeholder="Pencarian">
                <div class="input-group-addon">
                    <i class="fa fa-search"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="box box-info box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Data User</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <div class="box-body no-padding">
            <table class="table table-hover" wt-responsive-table>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama User</th>
                        <th>User Name</th>
                        <th>Kotak HP</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Kantor</th>
                        <th>Divisi</th>
                        <th>Posisi</th>
                        <!-- <th width="1%">Status</th> -->
                        <!-- <th width="1%"></th> -->
                    </tr>
                </thead>
                <tbody>
                    <tr dir-paginate="x in dataUser|filter:search|itemsPerPage:5" pagination-id="user">
                        <td>{{$index+1}}.</td>
                        <td>{{x.fullname}}</td>
                        <td>{{x.username}}</td>
                        <td>{{x.contact}}</td>
                        <td>{{x.email}}</td>
                        <td>{{x.roleName}}</td>
                        <td>{{x.companyName}}</td>
                        <td>{{x.departmentName}}</td>
                        <td>{{x.positionName}}</td>
                        <!-- <td>
                            <span ng-if="x.status == 1" class="label label-success">aktif</span>
                            <span ng-if="x.status == 0" class="label label-danger">nonaktif</span>
                        </td> -->
                        <!-- <td>
                            <div class="dropdown">
                                <button class="btn btn-xs btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Pilihan
                                <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                <li><a href="#!user/edit/{{x.id}}">Edit</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a ng-click="delete(x)">Hapus</a></li>
                                </ul>
                            </div>
                        </td> -->
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="text-right">
        <dir-pagination-controls pagination-id="user"></dir-pagination-controls>
    </div>

</section>