<section class="content-header">
    <h1>
        Dokumen - New
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Dokumen</li>
        <li class="active">New</li>
    </ol>
</section>

<section class="content">
    <form ng-submit="post()">
        <div class="box box-info box-solid">
            <div class="box-header">
                <h3 class="box-title">Tambah Data Dokumen</h3>
            </div>
            <div class="box-body">

                <!-- <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Sifat Dokumen</label>
                            <select required ng-model="form.sifat" class="form-control input-lg">
                                <option value="">Pilih Sifat Dokumen</option>
                                <option value="{{x.id}}" ng-repeat="x in document_sifat">{{x.name}}</option>
                            </select>
                        </div>
                    </div>
                </div> -->

                <div class="row" style="margin-top:25px;">
                    <!-- <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Divisi</label>
                            <select required ng-model="form.divisi" class="form-control input-lg">
                                <option value="">Pilih Sifat Dokumen</option>
                                <option value="{{x.id}}" ng-repeat="x in dataCabang">{{x.name}}</option>
                            </select>
                        </div>
                    </div> -->
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Aturan Dokumen</label>
                            <select required ng-model="aturanData" ng-change="aturanChange(aturanData)" class="form-control input-lg">
                                <option value="">Pilih Aturan Dokumen</option>
                                <option ng-value="x" ng-repeat="x in document_aturan">{{x.name}}</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top:35px;">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Nama Dokumen</label>
                            <input required ng-model="form.title" type="text" class="form-control input-lg">
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Nomor Surat {{form.aturan.id}}</label>
                            <input ng-model="form.no_surat" type="text" class="form-control input-lg">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Deskripsi Dokumen</label>
                            <textarea required ng-model="form.description" class="form-control input-lg" rows="5"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top:35px; margin-bottom:35px;">
                    <div class="col-md-12">
                        <h3><b>Uploads file Dokumen Anda</b></h3>
                        <p style="margin-bottom:25px;">klik kotak dibawah ini atau drag file ke kotak.</p>

                        <div class="dropzone" options="dzOptions" callbacks="dzCallbacks" methods="dzMethods" ng-dropzone></div>
                    </div>
                </div>

            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg">Simpan Data</button>
            </div>
        </div>
    </form>


</section>