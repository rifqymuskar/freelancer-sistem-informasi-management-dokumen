<style>
    .container{
        width:80%;
        margin:auto;
    }
    .image{
        width:35%;
    }
    p{
        font-size:0.9rem;
    }
    p.mini{
        font-size:0.7rem;
    }
</style>

<div style="width:80%; margin:auto;">
    <br>
    <img style="width:200px; height:auto;" src="https://mgsulawesi.co.id/public/assets/images/mglogo.png" class="img-responsive">

    <p style="font-size:0.9rem;">Terimakasih! Pengajuan test drive anda telah diterima, dengan biodata:</p>
    <br>
    <span style="font-size:0.9rem; margin-bottom:0px!important; padding-bottom:0px!important;">Bapak/Ibu : <b><?=$full_name?></b></span><br>
    <span style="font-size:0.9rem; margin-bottom:0px!important; padding-bottom:0px!important;">Rencana test Drive : <b><?=$date?></b></span><br>
    <span style="font-size:0.9rem; margin-bottom:0px!important; padding-bottom:0px!important;">Ingin mencoba Mobil : <b>Morris Garage <?=$units?></b></span><br>
    <br>
    <br>
    <br>
    <span style="font-size:0.9rem; margin-bottom:0px!important; padding-bottom:0px!important;">salam hangat</span>
    <br>
    <span style="font-size:0.9rem; margin-bottom:0px!important; padding-bottom:0px!important;"><b>Morris Garage Sulawesi Team</b></span>
    <br>
    <span style="font-size:0.9rem; margin-bottom:0px!important; padding-bottom:0px!important;"><a href="https://mgsulawesi.co.id">www.mgsulawesi.co.id</a></span>

    <br>
    <br>
    <br>
    <br>

    <p style="font-size:0.7rem;">MG was born in the UK and now it has been revived and expanded globally. It holds a global reputation in excellence and innovations with a nearly-century-old of experience. MG is currently based in Longbridge, Birmingham, England. And we hold our BritDynamic identity with pride as it is one of our key differentiators among other British or European cars. It is a standard applied to all of our car production that covers: UK design, precise handling, class leading power, & uncompromising safety</p>

    <p style="font-size:0.7rem;">Informasi lebih lanjut kunjungi website resmi kami di : <a href="https://mgsulawesi.co.id">www.mgsulawesi.co.id</a></p>
</div>