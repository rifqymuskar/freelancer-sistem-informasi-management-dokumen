<style>
    .container {
        width: 80%;
        margin: auto;
    }

    .image {
        width: 35%;
    }

    p {
        font-size: 0.9rem;
    }

    p.mini {
        font-size: 0.7rem;
    }
</style>

<div style="width:80%; margin:auto;">
    <br>
    <p>logo Kantor</p>

    <h5 style="font-size:0.9rem;">We got your password reset request!</h5>
    <p>Hey there, we received your password reset request. Please click below to reset your password</p>
    <a href="<?= $url ?>">Reset Password</a>
    <br>

</div>