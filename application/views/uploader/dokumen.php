<section class="content-header">
    <h1>
    Dokumen
    <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
    <li>Home</li>
    <li class="active">Dokumen</li>
    </ol>
</section>

<section class="content">

    <div class="row" style="margin-bottom:15px; margin-top:15px;">
        <div class="col-md-3" style="padding-right:5px;">
            <a href="#!dokumen/new" class="btn btn-primary btn-flat">Tambah Dokumen</a>
        </div>
        <div class="col-md-3" style="padding-right:5px; padding-left:5px;">
            <select ng-change="divisiChange()" class="form-control" ng-model="search.company">
                <option value="">Pilih Cabang</option>
                <option value="{{x.id}}" ng-repeat="x in dataCabang">{{x.name}}</option>
            </select>
        </div>
        <div class="col-md-3" style="padding-right:5px;">
            <select class="form-control" ng-model="search.aturan">
                <option value="">Pilih Jenis Aturan</option>
                <option value="{{x.kode}}" ng-repeat="x in dataAturan" ng-show="x.company == search.company">{{x.name}}</option>
            </select>
        </div>
        <div class="col-md-3" style="padding-left:5px; padding-right:5px;"></div>
        <div class="col-md-3" style="padding-left:5px;">
            <div class="input-group">
                <input type="text" class="form-control" value="" ng-model="search.$" placeholder="Pencarian">
                <div class="input-group-addon">
                <i class="fa fa-search"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="box box-info box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Data Dokumen</h3>

            <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <div class="box-body no-padding">
            
            <table class="table table-hover" wt-responsive-table>
                <thead>
                    <tr class="info">
                        <!-- <td ng-click="sort('name')">Name
							<span class="glyphicon sort-icon" ng-show="sortKey=='name'" ng-class="{'glyphicon-chevron-up':sortReverse,'glyphicon-chevron-down':!sortReverse}"></span>
						</td> -->
                        <td width="5%" ng-click="sort('num')"><b>No</b>
							<span style="padding-left:10px;" ng-class="{'fa fa-angle-up':sortReverse,'fa fa-angle-down':!sortReverse}"></span>
						</td>
                        <td width="20%" ng-click="sort('aturanName')"><b>Jenis Aturan</b>
							<span style="padding-left:10px;" ng-class="{'fa fa-angle-up':sortReverse,'fa fa-angle-down':!sortReverse}"></span>
						</td>
                        <td width="10%" ng-click="sort('no_surat')"><b>Nomor</b>
							<span style="padding-left:10px;" ng-class="{'fa fa-angle-up':sortReverse,'fa fa-angle-down':!sortReverse}"></span>
						</td>
                        <td width="6%" ng-click="sort('tahun')"><b>Tahun</b>
							<span style="padding-left:10px;" ng-class="{'fa fa-angle-up':sortReverse,'fa fa-angle-down':!sortReverse}"></span>
						</td>
                        <td width="15%"><b>Judul Dokumen</b></td>
                        <td><b>Ringkasan</b></td>
                        <td width="15%"><b>Divisi</b></td>
                        <td width="1%"><b></b></td>
                        <td width="1%"></td>
                    </tr>
                </thead>
                <tbody>
                    <tr dir-paginate="x in dataDocument|filter:search|orderBy:sortKey:sortReverse|itemsPerPage:5" pagination-id="dokumen">
                        <td width="1%">{{x.num}}.</td>
                        <td>{{x.aturanName}}</td>
                        <td>{{x.no_surat}}</td>
                        <td>{{x.tahun}}</td>
                        <td>{{x.title}}</td>
                        <td><span ng-bind-html="x.description | limitHtml: 100"></span></td>
                        <td>{{x.divisiName}}</td>
                        <td>
                            <span class="label label-success" ng-if="x.status == 1">Aktif</span>
                            <span class="label label-danger" ng-if="x.status == 0">Non Aktif</span>
                        </td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-xs btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Pilihan
                                <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                <li ng-click="details(x)"><a>Details</a></li>
                                <li><a href="#!dokumen/edit/{{x.id}}">Edit</a></li>
                                <!-- <li role="separator" class="divider"></li>
                                <li ng-click="nonaktif(x)" ng-show="x.status == 1"><a>Non Aktif</a></li>
                                <li ng-click="aktif(x)" ng-show="x.status == 0"><a>Aktif</a></li> -->
                                <!-- <li role="separator" class="divider"></li>
                                <li><a href="">Download PDF</a></li>
                                <li><a href="">Print</a></li> -->
                                <!-- <li><a>Konfirmasi pembayaran</a></li> -->
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            
        </div>
    </div>

    <div class="text-right">
        <dir-pagination-controls pagination-id="dokumen"></dir-pagination-controls>
    </div>

</section>

<!-- Modal -->
<div class="modal fade" id="details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-full" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Details Data Dokumen #{{dataHead.id}}</h4>
      </div>
      <div class="modal-body no-padding">

        <div style="padding:10px; padding-bottom:5px;">
            <p><b>Informasi Dokumen</b></p>
        </div>

        <table class="table table-hover table-striped">
            <tbody>
                <tr>
                    <td>Jenis Aturan</td>
                    <td>{{dataHead.aturanName}}</td>
                </tr>
                <tr>
                    <td>Nomor</td>
                    <td>{{dataHead.no_surat}}</td>
                </tr>
                <tr>
                    <td>Tahun</td>
                    <td>{{dataHead.tahun}}</td>
                </tr>
                <tr>
                    <td>Judul Dokumen</td>
                    <td>{{dataHead.title}}</td>
                </tr>
                <tr>
                    <td>Divisi</td>
                    <td>{{dataHead.divisiName}}</td>
                </tr>
            </tbody>
        </table>

        <div style="padding:15px;">
            <div style="padding:10px; width:100%; height:350px; overflow-y:scroll; border:1px solid #C1C1C1;">
                <span ng-bind-html="dataHead.descriptionFull"></span>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div ng-show="dataDetails.length > 0" style="padding:10px;">
                    <p><b>Dokumen file</b></p>

                    <div class="info-box bg-green" ng-repeat="x in dataDetails">
                        <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">{{x.name}}</span>
                        <span class="info-box-number">{{x.file_size}} KB</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                        </div>
                        <span class="progress-description">
                                {{x.file_type}}
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>

                </div>
            </div>
        </div>

        <!-- <table class="table table-hover table-striped">
            <tbody>
                <tr ng-repeat="x in dataDetails">
                    <td>{{x.file_type}}</td>
                    <td>{{x.file_size}} KB</td>
                    <td>{{x.name}}</td>
                    <td style="padding-left:0px;" width="1%"><button ng-click="preview(x)" class="btn btn-primary btn-flat btn-xs">Preview</button></td>
                    <td style="padding-left:0px;" width="1%"><a ng-href="./public/uploads/document/{{x.name}}" download="{{x.name}}" ng-click="preview(x)" class="btn btn-primary btn-flat btn-xs">Download</a></td>
                    <td style="padding-left:0px;" width="1%"><button ng-click="print(x)" class="btn btn-primary btn-flat btn-xs">Print</button></td>
                </tr>
            </tbody>
        </table> -->

      </div>
      <div class="modal-footer">
        <span ng-repeat="x in dataDetails">
            <button type="button" ng-click="preview(x)" class="btn btn-info pull-left btn-flat">Preview</button>
            <a ng-href="./public/uploads/document/{{x.name}}" download="{{x.name}}" class="btn btn-info pull-left btn-flat">Download</a>
            <button ng-click="print(x)" type="button" class="btn btn-info pull-left btn-flat">Print</button>
        </span>
        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="preview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body no-padding">

        <iframe style="width:100%; height:80vh;" ng-src="{{trustSrc(dataHead.name)}}" title="W3Schools Free Online Web Tutorials"></iframe>

      </div>
    </div>
  </div>
</div>