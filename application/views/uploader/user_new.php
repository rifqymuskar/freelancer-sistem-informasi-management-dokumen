<section class="content-header">
    <h1>
        User - {{title}}
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li>User</li>
        <li class="active">{{title}}</li>
    </ol>
</section>

<section class="content">
    <form ng-submit="put()">
        <div class="box box-info box-solid">
            <div class="box-header">
                <h3 class="box-title">Tambah Data Dokumen</h3>
            </div>
            <div class="box-body">

                <div class="row" style="margin-top:35px;">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Username</label>
                            <input required ng-model="form.username" type="text" class="form-control input-lg">
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Password</label>
                            <input ng-required="title == 'New'" ng-model="form.password" type="password" class="form-control input-lg">
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top:25px;">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Nama lengkap</label>
                            <input required ng-model="form.fullname" type="text" class="form-control input-lg">
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Kontak HP</label>
                            <input required ng-model="form.contact" type="text" class="form-control input-lg">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Email</label>
                            <input required ng-model="form.email" type="text" class="form-control input-lg">
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top:25px;">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Role</label>
                            <select required ng-model="form.role" class="form-control input-lg">
                                <option value="">Pilih Role</option>
                                <option value="{{x.id}}" ng-repeat="x in dataRole">{{x.name}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Kantor</label>
                            <select required ng-model="form.company" class="form-control input-lg">
                                <option value="">Pilih Kantor</option>
                                <option value="{{x.id}}" ng-repeat="x in dataCabang">{{x.name}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom:25px;">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Divisi</label>
                            <select required ng-model="form.department" class="form-control input-lg">
                                <option value="">Pilih Divisi</option>
                                <option value="{{x.id}}" ng-repeat="x in dataDivisi" ng-if="x.company_id == form.company">{{x.name}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Posisi</label>
                            <select required ng-model="form.position" class="form-control input-lg">
                                <option value="">Pilih Posisi</option>
                                <option value="{{x.id}}" ng-repeat="x in dataPosisi">{{x.name}}</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg">Simpan Data</button>
            </div>
        </div>
    </form>
</section>