<!DOCTYPE html>
<html ng-app="RifqyMuskar">
    <head>

    <link rel="icon" href="/public/assets/images/favicon.ico" type="image/gif" sizes="16x16">

    <title>Panel Admin - Digilib JR</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">

        <!-- AngularJS file -->
        <?=libraryJS('jquery/dist/jquery.min.js')?>

        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular-route.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular-animate.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular-sanitize.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ckeditor/1.0.3/angular-ckeditor.min.js"></script>
        <script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/angular-utils-pagination@0.11.1/dirPagination.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

        <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>

        <script src="public/node_modules/chart.js/dist/Chart.min.js"></script>
        <script src="public/node_modules/angular-chart.js/dist/angular-chart.min.js"></script>

        <?=angularjs('assets/js/select2.js')?>
        <?=angularjs('angularJS/'.$role.'/app.js?version='.$app_version->version)?>
        <?=angularjs('angularJS/'.$role.'/controller.js?version='.$app_version->version)?>
        <?=angularjs('angularJS/'.$role.'/function.js?version='.$app_version->version)?>

        <!-- Admin LTE 2 -->
        <?=libraryCSS('bootstrap/dist/css/bootstrap.min.css')?>
        <?=libraryCSS('font-awesome/css/font-awesome.min.css')?>
        <?=libraryCSS('Ionicons/css/ionicons.min.css')?>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet">
        <!-- LIBRARY DATEPICKER -->
        <?=libraryCSS('bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')?>

        <?=css('adminLTE/css/AdminLTE.min.css')?>
        <?=css('adminLTE/css/skins/_all-skins.min.css')?>

        <link href="https://printjs-4de6.kxcdn.com/print.min.css" rel="stylesheet">
        
        <!-- DROPZONE -->
        <script src="https://rawgit.com/enyo/dropzone/master/dist/dropzone.js"></script>
        <?=js('assets/js/ng-dropzone.js')?>
        <link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">

        <!-- RESPONSIVE TABLE -->
        <?=css('assets/css/angular-responsive-tables.min.css')?>
        <?=js('assets/js/angular-responsive-tables.min.js')?>

        <?=css('assets/css/style.css')?>
        <?=css('assets/css/responsive.css')?>

        <?=pluginCSS('iCheck/square/blue.css')?>
        <?=pluginCSS('jquery-toast-plugin-master/dist/jquery.toast.min.css')?>

        <!-- Google Font -->
        <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <!-- Admin LTE App -->
        <?=libraryJS('bootstrap/dist/js/bootstrap.min.js')?>
        <?=libraryJS('fastclick/lib/fastclick.js')?>

        <?=js('adminLTE/js/adminlte.js')?>
        <?=js('assets/js/style.js')?>
        <?=libraryJS('jquery-slimscroll/jquery.slimscroll.min.js')?>
        <!-- LIBRARY DATEPICKER -->
        <?=libraryJS('moment/min/moment.min.js')?>
        <?=libraryJS('bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>

        <?=pluginJS('iCheck/icheck.min.js')?>
        <?=pluginJS('jquery-toast-plugin-master/dist/jquery.toast.min.js')?>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.qrcode/1.0/jquery.qrcode.min.js"></script>

        <!-- Sweet Alert 1 -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <!-- PUT HERE OTHER LIBRARY OR FUNCTION -->

    </head>
<body class="skin-blue fixed sidebar-mini">
 
<!-- Loading data -->
<div class="loading active">
    <div>
        <div class="sk-chase">
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
        </div>
    </div>
</div>

<div class="wrapper">
  <!-- navbar panel admin -->
  <header class="main-header">
      <!-- Logo -->
      <a href="index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>DJR</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Digilib JR</b></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav" ng-controller="sidebar">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?=site_url('public/assets/images/profile.png')?>" class="user-image" alt="User Image">
                <span class="hidden-xs">{{session.fullname}}</span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="<?=site_url('public/assets/images/profile.png')?>" class="img-circle" alt="User Image">
                  <p>
                    {{session.fullname}}
                    <small>{{session.departmentName}} - {{session.companyName}}</small>
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <!-- <a href="#" class="btn btn-default btn-flat">Profile</a> -->
                  </div>
                  <div class="pull-right">
                    <a href="" class="btn btn-default btn-flat" ng-click="logoutSession()">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>

      </nav>
  </header>
  <aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar" style="height: auto;">
      <!-- Sidebar user panel -->
      <div class="user-panel" style="min-height:60px;">
      <div class="pull-left image">
          <img src="<?=site_url('public/assets/images/profile.png')?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
          <p style="font-size:1rem;">{{session.fullname}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> {{session.roleName}}</a>
      </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu tree" data-widget="tree" ng-controller="sidebar">
          <li class="header">Navigasi Umum</li>
          <li ng-class="getClass('/dashboard')">
            <a href="#!dashboard">
            <i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Dashboard</span>
              <span class="pull-right-container">
              </span>
            </a>
          </li>
          <!-- <li ng-class="getClass('/profile')">
            <a href="#!profile" >
            <i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Profile</span>
              <span class="pull-right-container">
              </span>
            </a>
          </li> -->
          <!-- <li class="treeview" ng-class="getShow(['/perusahaan', '/perusahaan/new'])">
            <a href="">
              <i class="fa fa-dot-circle-o"></i> <span>Data Perusahaan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li ng-class="getClass('/perusahaan')"><a href="#!perusahaan"><i class="fa fa-circle-o"></i> List Perusahaan</a></li> 
              <li ng-class="getClass('/perusahaan/new')"><a href="#!perusahaan/new"><i class="fa fa-circle-o"></i> Tambah Perusahaan</a></li> 
            </ul>
          </li> -->

          <!-- <li class="treeview" ng-class="getShow(['/user', '/divisi'])">
            <a href="">
              <i class="fa fa-dot-circle-o"></i> <span>File Admin</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li ng-class="getClass('/user')"><a href="#!user"><i class="fa fa-circle-o"></i> User</a></li> 
              <li ng-class="getClass('/divisi')"><a href="#!divisi"><i class="fa fa-circle-o"></i> Divisi</a></li> 
            </ul>
          </li> -->

          <!-- <li class="treeview" ng-class="getShow(['/pusat', '/cabang', '/divisi/new'])">
            <a href="">
              <i class="fa fa-dot-circle-o"></i> <span>Data Divisi</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li ng-class="getClass('/pusat')"><a href="#!pusat"><i class="fa fa-circle-o"></i> Kantor Pusat</a></li> 
              <li ng-class="getClass('/cabang')"><a href="#!cabang"><i class="fa fa-circle-o"></i> Kantor Cabang</a></li> 
              <li ng-class="getClass('/divisi/new')"><a href="#!divisi/new"><i class="fa fa-circle-o"></i> Tambah Divisi</a></li> 
            </ul>
          </li>
          <li class="treeview" ng-class="getShow(['/user', '/user/new'])">
            <a href="">
              <i class="fa fa-dot-circle-o"></i> <span>Data User</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li ng-class="getClass('/user')"><a href="#!user"><i class="fa fa-circle-o"></i> List User</a></li> 
              <li ng-class="getClass('/user/new')"><a href="#!user/new"><i class="fa fa-circle-o"></i> Tambah User</a></li> 
            </ul>
          </li> -->
          <!-- <li class="treeview" ng-class="getShow(['/dokumen'])">
            <a href="">
              <i class="fa fa-dot-circle-o"></i> <span>Management Dokumen</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li ng-class="getClass('/dokumen')"><a href="#!dokumen"><i class="fa fa-circle-o"></i> List Dokumen</a></li> 
            </ul>
          </li> -->

          <li class="header">Navigasi Lain</li>

          <!-- <li><a href=""><i class="fa fa-coffee"></i> <span>Tutorial Aplikasi</span></a></li> -->
          <li><a href="" ng-click="logoutSession()"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
      </ul>
  </section>
  <!-- /.sidebar -->
  </aside>

  <div class="content-wrapper" compile-after="appReady">
    <!-- Content Header (Page header) -->
    <ng-view></ng-view>
  </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs" id="notification">
    </div>
    <strong>Copyright © 2021 <a href="https://instagram.com/rifqymuskar">RifqyMuskar</a>.</strong> All rights
    reserved.
  </footer>

</div>

</body>
</html>