<div class="row">
  <div class="col-md-4 col-sm-5" style="padding-right:0px;">
    <div class="log-3">
      <div>
        <img class="img-responsive" style="width:35%; margin:0 auto;" src="/public/assets/images/jasaraharja.png">
        <div class="text-center">
          <h3 class="log-5">Digital Library - Jasa Raharja</h3>
          <p class="log-7" style="padding-top:10px; margin-bottom:25px;">Silahkan mengisi Username dan Password anda untuk dapat masuk ke Aplikasi</p>
        </div>
      </div>
      <form ng-submit="post()">
        <div class="form-group">
          <input autofocus required type="text" ng-model="form.user" class="form-control log-4 username" placeholder="Masukkan Username">
        </div>
        <div class="form-group">
          <input required type="password" ng-model="form.password" class="form-control log-4" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-1 log-6">Masuk sekarang</button>
        <p class="log-7" style="padding-top:10px;">Lupa password ? <a href="#!forgotpassword">Klik Disini</a></p>
      </form>
    </div>
  </div>
  <div class="col-md-8 col-sm-7" style="padding-left:0px;">
    <div class="log">
    </div>
  </div>
</div>

</div>