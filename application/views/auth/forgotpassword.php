<div class="row">
  <div class="col-md-4 col-sm-5" style="padding-right:0px;">

    <div class="log-3">
      <div>
        <img class="img-responsive" style="width:35%; margin:0 auto;" src="/public/assets/images/jasaraharja.png">
        <div class="text-center">
          <h5 class="log-5" style="margin-bottom:10px;">Reset Password</h5>
          <p class="log-7" style="padding-top:10px; margin-bottom:25px;">Silahkan mengisi alamat email user anda untuk melakukan reset password</p>
        </div>

        <form ng-submit="post()">
          <div class="form-group">
            <input autofocus required type="email" ng-model="form.user" class="form-control log-4 username" placeholder="Masukkan alamat email anda">
          </div>
          <button type="submit" class="btn btn-1 log-6">Submit</button>
          <div class="text-center" style="margin-top:15px;">
            <p class="log-7">Sudah memiliki user ? silahkan login melalui link ini <br> <a href="<?=site_url('/')?>">Login ke aplikasi</a></p>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-8 col-sm-7" style="padding-left:0px;">
    <div class="log">
    </div>
  </div>
</div>