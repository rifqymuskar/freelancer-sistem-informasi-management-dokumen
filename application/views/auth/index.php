<!DOCTYPE html>
<html ng-app="RifqyMuskar">
    <head>
        <title>Panel Admin - Digilib JR</title>

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <link rel="icon" href="/public/assets/images/favicon.ico" type="image/gif" sizes="16x16">

        <!-- AngularJS file -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular-route.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        
        <?=angularjs('angularJS/login/app.js?version='.$app_version->version)?>
        <?=angularjs('angularJS/login/controller.js?version='.$app_version->version)?>
        <?=angularjs('angularJS/login/function.js?version='.$app_version->version)?>

        <!-- Admin LTE 2 -->
        <?=libraryCSS('bootstrap/dist/css/bootstrap.min.css')?>
        <?=libraryCSS('font-awesome/css/font-awesome.min.css')?>
        <?=libraryCSS('Ionicons/css/ionicons.min.css')?>

        <?=css('adminLTE/css/AdminLTE.min.css')?>
        <?=css('adminLTE/css/skins/_all-skins.min.css')?>
        <?=css('angularJS/login/assets/css/style.css')?>

        <?=pluginCSS('iCheck/square/blue.css')?>
        <?=pluginCSS('jquery-toast-plugin-master/dist/jquery.toast.min.css')?>

        <!-- Google Font -->
        <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">



        <!-- Admin LTE App -->
        <?=libraryJS('jquery/dist/jquery.min.js')?>
        <?=libraryJS('bootstrap/dist/js/bootstrap.min.js')?>
        <?=libraryJS('fastclick/lib/fastclick.js')?>

        <?=js('adminLTE/js/adminlte.js')?>
        <?=js('angularJS/login/assets/js/style.js')?>
        <?=libraryJS('jquery-slimscroll/jquery.slimscroll.min.js')?>

        <?=pluginJS('iCheck/icheck.min.js')?>
        <?=pluginJS('jquery-toast-plugin-master/dist/jquery.toast.min.js')?>

        <!-- PUT HERE OTHER LIBRARY OR FUNCTION -->

    </head>
<!-- <body class="hold-transition login-page"> -->
<body>

<!-- Loading data -->
<div class="loading active">
    <div>
        <div class="sk-chase">
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
        </div>
    </div>
</div>

<!-- Login admin -->

<ng-view></ng-view>

</body>
</html>