<div class="row">
  <div class="col-md-4 col-sm-5" style="padding-right:0px;">

    <div class="log-3">
      <div>
        <img class="img-responsive" style="width:35%; margin:0 auto;" src="/public/assets/images/jasaraharja.png">
        <div class="text-center">
          <h5 class="log-5" style="margin-bottom:10px;">New Password</h5>
          <p class="log-7" style="padding-top:10px; margin-bottom:25px;">Silahkan mengisi Password baru anda.</p>
        </div>

        <form ng-submit="post()">
          <div class="form-group">
            <input autofocus required type="password" ng-model="form.user" class="form-control log-4 username" placeholder="Masukkan Password Baru Anda">
          </div>
          <button type="submit" class="btn btn-1 log-6">Update Password</button>
        </form>
      </div>
    </div>
    <div class="text-center">
      <p class="log-7">Sudah memiliki user ? silahkan login melalui link ini <br> <a href="<?=site_url('/')?>">Login ke aplikasi</a></p>
    </div>
  </div>
  <div class="col-md-8 col-sm-7" style="padding-left:0px;">
    <div class="log">
    </div>
    <p class="log-8">Developed and Maintenance Application by Iwan dan kawan-kawan</p>
  </div>
</div>