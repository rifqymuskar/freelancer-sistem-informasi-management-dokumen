<section class="content-header">
    <h1>
    Divisi - {{title}}
    <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
    <li>Home</li>
    <li>Divisi</li>
    <li class="active">New</li>
    </ol>
</section>

<section class="content">

    <form ng-submit="put()">

    <div class="box box-info box-solid">
        <div class="box-header">
            <h3 class="box-title">Tambah Data Divisi</h3>
        </div>
        <div class="box-body">
            
                <div class="row" style="margin-top:35px;">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Kantor Divisi</label>
                            <select ng-model="form.company_id" class="form-control input-lg">
                                <option value="">pilih kantor divisi</option>
                                <option value="1">Kantor Pusat</option>
                                <option value="2">Kantor Cabang</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Nama Divisi</label>
                            <input ng-model="form.name" type="text" class="form-control input-lg">
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Kode Divisi</label>
                            <input ng-model="form.kode" type="text" class="form-control input-lg">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Deskripsi Divisi</label>
                            <textarea ng-model="form.description" class="form-control input-lg" rows="5"></textarea>
                        </div>
                    </div>
                </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-lg">Simpan Data</button>
        </div>
    </div>

    </form>

</section>