<section class="content-header">
    <h1>
        Dokumen - {{titleHead}}
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Dokumen</li>
        <li class="active">New</li>
    </ol>
</section>

<section class="content">
    <form ng-submit="post()">
        <div class="box box-info box-solid">
            <div class="box-header">
                <h3 class="box-title">{{titleDoc}}</h3>
            </div>
            <div class="box-body">

                <div class="row" style="margin-bottom:35px;">
                    <div class="col-md-12">
                        <h4><b>Upload Dokumen Anda</b></h4>
                        <p style="margin-bottom:25px;">klik kotak dibawah ini atau drag file ke kotak.</p>

                        <div class="dropzone" options="dzOptions" callbacks="dzCallbacks" methods="dzMethods" ng-dropzone></div>

                        <p style="margin-top:25px;">File Dokumen akan tersimpan pada server setelah menekan tombol <strong>Simpan Data</strong>.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Kantor</label>
                            <select required ng-model="form.company" class="form-control input-lg">
                                <option value="">Pilih Kantor</option>
                                <option value="{{x.id}}" ng-repeat="x in dataCabang">{{x.name}}</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top:25px;">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Jenis Aturan</label>
                            <select required ng-model="form.aturan" ng-change="aturanChange()" class="form-control input-lg">
                                <option value="" disabled>Pilih Jenis Aturan</option>
                                <option ng-value="x.kode" ng-if="x.company == form.company" ng-repeat="x in document_aturan">{{x.name}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Divisi</label>
                            <select required ng-model="form.divisi" ng-change="aturanChange()" class="form-control input-lg">
                                <option value="">Pilih Divisi</option>
                                <option value="{{x.kode}}" ng-repeat="x in dataDivisi" ng-if="x.company_id == form.company">{{x.name}}</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top:35px;">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Nama Dokumen</label>
                            <input required ng-model="form.title" type="text" class="form-control input-lg">
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Nomor Surat {{form.aturan.id}}</label>
                            <input ng-model="form.no_surat" type="text" class="form-control input-lg">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Deskripsi Dokumen</label>
                            <textarea ckeditor="options" required ng-model="form.description" class="form-control input-lg" rows="5"></textarea>
                        </div>
                    </div>
                </div>

            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg btn-flat">Simpan Data</button>
            </div>
        </div>
    </form>


</section>