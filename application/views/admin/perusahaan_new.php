<section class="content-header">
    <h1>
        Kantor - New
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Kantor</li>
        <li class="active">New</li>
    </ol>
</section>

<section class="content">

    <div class="box box-info box-solid">
        <div class="box-body">

            <h3><b>Tambah Data Kantor</b></h3>
            <form>
                <div class="row" style="margin-top:35px;">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Nama Kantor</label>
                            <input type="text" class="form-control input-lg">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" class="form-control input-lg">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-lg">Simpan Data</button>
        </div>
    </div>

</section>