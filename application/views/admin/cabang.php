<section class="content-header">
    <h1>
    Kantor Cabang
    <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
    <li>Home</li>
    <li class="active">Kantor Cabang</li>
    </ol>
</section>

<section class="content">

    <div class="row" style="margin-bottom:15px; margin-top:15px;">
        <!-- <div class="col-md-3" style="padding-right:5px;">
            <select class="form-control">
                <option value="">Pilih Divisi</option>
                <option value="">Departemen HR</option>
                <option value="">Departemen IT</option>
            </select>
        </div>
        <div class="col-md-3" style="padding-left:5px; padding-right:5px;">
            <select class="form-control">
                <option value="">Pilih Jenis Aturan</option>
                <option value="">Aturan 1</option>
                <option value="">Aturan 2</option>
            </select>
        </div> -->
        <div class="col-md-9" style="padding-left:5px; padding-right:5px;"></div>
        <div class="col-md-3" style="padding-left:5px;">
            <input type="text" class="form-control" value="" ng-model="search.$" placeholder="Pencarian">
        </div>
    </div>

    <div class="box box-info box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Data Divisi Kantor Cabang</h3>

            <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <div class="box-body no-padding">
            <table class="table table-hover" wt-responsive-table>
                <thead>
                    <tr>
                        <th width="1%">No.</th>
                        <th>Kode Divisi</th>
                        <th>Nama Divisi</th>
                        <th>Jumlah User</th>
                        <!-- <th>Deskripsi</th> -->
                        <th>Dibuat</th>
                        <th width="1%"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr dir-paginate="x in dataDivisi|filter:search|itemsPerPage:5" pagination-id="data">
                        <td>{{$index+1}}</td>
                        <td>{{x.kode}}</td>
                        <td>{{x.name}}</td>
                        <td>{{x.total_user}}</td>
                        <td>{{x.create_at}}</td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-xs btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Pilihan
                                <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                <li><a href="#!divisi/edit/{{x.id}}">Edit</a></li>
                                <!-- <li ng-show="x.status == 1" ng-click="nonaktif(x)"><a href="">Non Aktif</a></li>
                                <li ng-show="x.status == 0" ng-click="aktif(x)"><a href="">Aktif</a></li> -->
                                <li role="separator" class="divider"></li>
                                <li><a ng-click="delete(x)">Hapus</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="text-right">
        <dir-pagination-controls pagination-id="data"></dir-pagination-controls>
    </div>

</section>