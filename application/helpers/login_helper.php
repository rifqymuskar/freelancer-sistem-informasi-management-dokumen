<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function angularjs($file = ''){
    return '<script src="'.base_url("public/").$file.'"></script>';
} 

// -------------------------------------------------

function css($file = ''){
    return '<link rel="stylesheet" href="'.base_url('public/').$file.'"/>';
} 
function libraryCSS($file = ''){
    return '<link rel="stylesheet" href="'.base_url('public/adminLTE/library/').$file.'"/>';
} 
function pluginCSS($file = ''){
    return '<link rel="stylesheet" href="'.base_url('public/adminLTE/plugins/').$file.'"/>';
} 

// ------------------------------------------------

function js($file = ''){
    return '<script src="'.base_url("public/").$file.'"></script>';
} 
function libraryJS($file = ''){
    return '<script src="'.base_url("public/adminLTE/library/").$file.'"></script>';
}
function pluginJS($file = ''){
    return '<script src="'.base_url("public/adminLTE/plugins/").$file.'"></script>';
}  

function urlSekarang(){
    $url = base_url();
    $url = str_replace("https://", "", $url);
    $url = str_replace("http://", "", $url);
    $url = str_replace("/", "", $url);
    return $url;
}