let stateCheck = setInterval(() => {
    if (document.readyState === 'complete') {
      clearInterval(stateCheck);
      $(".loading").removeClass('active')
      // document ready
    }
  }, 100);

  $('.loading').bind('ajaxStart', function(){
    $(this).addClass('active')
  }).bind('ajaxStop', function(){
    $(this).removeClass('active')
  });

  Dropzone.autoDiscover = false;