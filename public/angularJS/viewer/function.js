$(document).on('hidden.bs.modal', function (event) {
    if ($('.modal:visible').length) //check if any modal is open
    {
      $('body').addClass('modal-open');//add class to body
    }
});

function notification(heading, text, icon){
    $.toast({
        heading: heading,
        text: text,
        icon: icon,
        loader: true,        // Change it to false to disable loader
        showHideTransition: 'plain',
        hideAfter: 5000,
        loaderBg: '#9EC600'  // To change the background
    })
}