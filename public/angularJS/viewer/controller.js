app.controller('dashboard', function($rootScope, $scope, $sce, $filter, $route, $http, request){

  $scope.sortKey = 'no_surat';
  $scope.sortReverse = false;

  $scope.sort = function(key){
    $scope.sortReverse = ($scope.sortKey == key) ? !$scope.sortReverse : $scope.sortReverse;
    $scope.sortKey = key;
  }

  request.get("api/document").then(function(response){
    data = response.data.data
    num = 0
    for(x in data){
      num = num+1
      data[x].num = num
      check = $filter('filter')($scope.dataAturan, {'kode':data[x].aturan})[0]  

      if(check){
        data[x].aturanName = check.name
      }else{
        data[x].aturanName = '-'
      }

      check1 = $filter('filter')($scope.dataDivisi, {'kode':data[x].divisi})[0]
      if(check1){
        data[x].divisiName = check1.name
      }else{
        data[x].divisiName = '-'
      }

    }
    $scope.dataDocument = data
  })

  $scope.divisiChange = function(){
    $scope.search.aturan = ""
  }

  $scope.nonaktif = function(data){
    $scope.form = {}
    $scope.form.id = data.id
    $scope.form.status = 0
    request.put("api/document", $scope.form).then(function(response){
      checkResponse(response, $route)
    })
  }
  $scope.aktif = function(data){
    $scope.form = {}
    $scope.form.id = data.id
    $scope.form.status = 1
    request.put("api/document", $scope.form).then(function(response){
      checkResponse(response, $route)
    })
  }

  $scope.details = function(data){
    $scope.dataHead = data

    request.get("api/getDataByKey/document_file", {
      "key" : "document_id",
      "value" : $scope.dataHead.id
    }).then(function(response){
      $scope.dataDetails = response.data.data
    })

    $("#details").modal("show")
  }

  $scope.print = function(data){
    if(data.file_type == 'image/jpeg' || data.file_type == 'image/jpg' || data.file_type == 'image/png'){
      printJS({
        printable: './public/uploads/document/'+data.name,
        type: 'image'
      })
    }
    if(data.file_type == 'application/pdf'){
      printJS({
        printable: './public/uploads/document/'+data.name,
        type: 'pdf'
      })
    }
  }

  $scope.preview = function(data){
    $scope.dataHead = data
    $("#preview").modal("show")
  }
  $scope.trustSrc = function(src) {
    console.log(src)
    return $sce.trustAsResourceUrl('https://digilib.simas-jr.com/public/uploads/document/'+src);
  }
  })
  
  // USER
  app.controller('user_new', function($rootScope, $scope, $location, $http, request){
  
    $scope.title = "New"
  
    $scope.form = {}
    $scope.put = function(){
      request.put("api/users", $scope.form).then(function(response){
        if(response.data.status == 'success'){
          $scope.form = {}
          $location.path("user")
        }
        notification(response.data.status, response.data.message, response.data.status)
      })
    }
  })
  
  app.controller('user_edit', function($rootScope, $scope, $routeParams, $http, request){
  
    request.get("api/users", {id : $routeParams.id}).then(function(response){
      console.log(response)
      $scope.form = response.data.data[0]
      $scope.form.password = ""
    })
  
    $scope.title = "Edit"
  
    $scope.put = function(){
      request.put("api/users", $scope.form).then(function(response){
        notification(response.data.status, response.data.message, response.data.status)
      })
    }
  })
  
  app.controller('user', function($rootScope, $scope, $filter, $route, $http, request){
  
    $scope.changePerusahaan = function(){
      $scope.search.department = ""
    }
  
    $scope.getUser = function(){
      request.get("api/users", {status : "1"}).then(function(response){
        data = response.data.data
        for(x in data){
          data[x].roleName = $filter('filter')($scope.dataRole, {'id':data[x].role})[0].name  
          data[x].companyName = $filter('filter')($scope.dataCabang, {'id':data[x].company})[0].name 
  
          check2 = $filter('filter')($scope.dataDivisi, {'id':data[x].department})[0]
          if(check2){
            data[x].departmentName = check2.name  
          }
          
          data[x].positionName = $filter('filter')($scope.dataPosisi, {'id':data[x].position})[0].name  
        }
        $scope.dataUser = data
      })
    }
    $scope.getUser()
  
    $scope.nonaktif = function(data){
      $scope.data = data
      $scope.data.status = 0
  
      request.put("api/users/", $scope.data).then(function(response){
        checkResponse(response, $route)
      })
  
    }
    $scope.aktif = function(data){
      $scope.data = data
      $scope.data.status = 1
  
      request.put("api/users/", $scope.data).then(function(response){
        checkResponse(response, $route)
      })
  
    }
  
    $scope.delete = function(data){
  
      swal({
        title: "Are you sure?",
        text: "menghapus data ini akan menghilang data.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        closeOnClickOutside: false
      })
      .then((willDelete) => {
        if (willDelete) {
          request.delete("api/users"+data.id).then(function(response){
            checkResponse(response, $route)
          })
        }
      });
  
    }
  
  })
  
  // DIVISI
  app.controller('divisi_new', function($rootScope, $scope, $location, $http, request){
  
    $scope.form = {}
    $scope.title = "New"
  
    $scope.form = {}
    $scope.put = function(){
      request.put("api/users_department", $scope.form).then(function(response){
        if(response.data.status == 'success'){
          $scope.form = {}
          $location.path("divisi")
        }
        notification(response.data.status, response.data.message, response.data.status)
      })
    }
  })
  
  app.controller('divisi_edit', function($rootScope, $scope, $routeParams, $http, request){
  
    request.get("api/users_department", {id : $routeParams.id}).then(function(response){
      $scope.form = response.data.data[0]
    })
  
    $scope.title = "Edit"
  
    $scope.put = function(){
      request.put("api/users_department", $scope.form).then(function(response){
        notification(response.data.status, response.data.message, response.data.status)
      })
    }
  })
  app.controller('divisi', function($rootScope, $scope, $filter, $route, $http, request){
  
    $scope.getData = function(){
      request.get("api/getDivisi/users_department").then(function(response){
        data = response.data.data
        for(x in data){
          data[x].companyName = $filter('filter')($rootScope.dataCabang, {'id':data[x].company_id})[0].name  
        }
        $scope.dataDivisi = data
      })
    }
    $scope.getData()
  
    $scope.delete = function(data){
  
      swal({
        title: "Are you sure?",
        text: "menghapus data ini akan menghilang data.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        closeOnClickOutside: false
      })
      .then((willDelete) => {
        if (willDelete) {
          request.delete("api/users_department/"+data.id).then(function(response){
            checkResponse(response, $route)
          })
        }
      });
  
    }
  
  })
  app.controller('cabang', function($rootScope, $scope, $filter, $route, $http, request){
  
    $scope.getData = function(){
      request.get("api/getDivisi/users_department", {company_id : '2'}).then(function(response){
        data = response.data.data
        $scope.dataDivisi = data
      })
    }
    $scope.getData()
  
    $scope.delete = function(data){
  
      swal({
        title: "Are you sure?",
        text: "menghapus data ini akan menghilang data.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        closeOnClickOutside: false
      })
      .then((willDelete) => {
        if (willDelete) {
          request.delete("api/users_department/"+data.id).then(function(response){
            checkResponse(response, $route)
          })
        }
      });
    }
  })
  app.controller('dokumen_new', function($rootScope, $scope, $filter, $location, $route, $http, request){
    array = []

    $scope.form = {}
    request.get('api/session').then(function (response) {
      $rootScope.session = response.data

      if($rootScope.session.company == 1){
        request.get("api/document_aturan").then(function(response){
          $scope.document_aturan = response.data.data
        })
      }else{
        request.get("api/getDataByKey/document_aturan",{
          'key' : 'company',
          'value' : 2
        }).then(function(response){
          $scope.document_aturan = response.data.data
        })
      }

    })
    request.get('api/getDocumentId/').then(function(response){
      $scope.idDocument = response.data.data
    })
    
    $scope.aturanChange = function(){
  
      var d = new Date();
      var n = d.getFullYear();
  
      $scope.form.no_surat = $scope.aturanData.kode + '/' + $rootScope.session.departmentKode + "/" + $scope.idDocument
      $scope.form.aturan = $scope.aturanData.kode
    }
  
    request.get("api/document_sifat").then(function(response){
      $scope.document_sifat = response.data.data
    })
  
    //Set options for dropzone
      //Visit http://www.dropzonejs.com/#configuration-options for more options
      $scope.dzOptions = {
          url : 'api/documentFile',
      headers: {
              'authentication': 'rifqymuskar'
      },
          paramName : 'photo',
          acceptedFiles : '.jpg,.jpeg,.png,.pdf,.doc,.docx,.xlsx,.xls,.csv',
          addRemoveLinks : true,
      removedfile: function(file) {
        $scope.newFile = file
        request.delete("api/document_file/"+file.id).then(function(response){
          file.previewElement.remove();
  
          const index = array.indexOf($scope.newFile.id);
          if (index > -1) {
            array.splice(index, 1);
          }
  
          notification(response.data.status, response.data.message, response.data.status)
        }) 
      }
      };
      
      $scope.dzCallbacks = {
          'addedfile' : function(file){
              $scope.newFile = file;
          },
          'success' : function(file, xhr){
        if(xhr.status != 'failed'){
          $scope.newFile.id = xhr.data.id
          data = xhr
          if(data.status == 'success'){
            array.push(data.data.id)
            // $scope.getDataGallery()
          }
        }
        notification(data.status, data.message, data.status)
          }
      };
  
      $scope.dzMethods = {};
      $scope.removeNewFile = function(){
      $scope.dzMethods.removeFile($scope.newFile); //We got $scope.newFile from 'addedfile' event callback
      console.log($scope.newFile)
      }
  
    $scope.post = function(){
      $scope.form.file = array
      request.put("api/document", $scope.form).then(function(response){
        if(response.data.status == 'success'){
          $location.path('/dokumen')
        }
        notification(response.data.status, response.data.message, response.data.status)
      })
    }
  
  })
  app.controller('dokumen', function($rootScope, $scope, $sce, $filter, $route, $http, request){
    request.get("api/document").then(function(response){
      data = response.data.data
      num = 0
      for(x in data){
        num = num+1
        data[x].num = num
        check = $filter('filter')($scope.dataAturan, {'kode':data[x].aturan})[0]  

        if(check){
          data[x].aturanName = check.name
        }else{
          data[x].aturanName = '-'
        }

        check1 = $filter('filter')($scope.dataDivisi, {'kode':data[x].divisi})[0]
        if(check1){
          data[x].divisiName = check1.name
        }else{
          data[x].divisiName = '-'
        }

      }
      $scope.dataDocument = data
    })
  
    $scope.divisiChange = function(){
      $scope.search.aturan = ""
    }
  
    $scope.nonaktif = function(data){
      $scope.form = {}
      $scope.form.id = data.id
      $scope.form.status = 0
      request.put("api/document", $scope.form).then(function(response){
        checkResponse(response, $route)
      })
    }
    $scope.aktif = function(data){
      $scope.form = {}
      $scope.form.id = data.id
      $scope.form.status = 1
      request.put("api/document", $scope.form).then(function(response){
        checkResponse(response, $route)
      })
    }
  
    $scope.details = function(data){
      $scope.dataHead = data
  
      request.get("api/getDataByKey/document_file", {
        "key" : "document_id",
        "value" : $scope.dataHead.id
      }).then(function(response){
        $scope.dataDetails = response.data.data
      })
  
      $("#details").modal("show")
    }

    $scope.print = function(data){
      if(data.file_type == 'image/jpeg' || data.file_type == 'image/jpg' || data.file_type == 'image/png'){
        printJS({
          printable: './public/uploads/document/'+data.name,
          type: 'image'
        })
      }
      if(data.file_type == 'application/pdf'){
        printJS({
          printable: './public/uploads/document/'+data.name,
          type: 'pdf'
        })
      }
    }
  
    $scope.preview = function(data){
      $scope.dataHead = data
      $("#preview").modal("show")
    }
    $scope.trustSrc = function(src) {
      console.log(src)
      return $sce.trustAsResourceUrl('https://digilib.simas-jr.com/public/uploads/document/'+src);
    }

    $scope.sortKey = 'no_surat';
    $scope.sortReverse = false;

    $scope.sort = function(key){
      $scope.sortReverse = ($scope.sortKey == key) ? !$scope.sortReverse : $scope.sortReverse;
      $scope.sortKey = key;
    }

  })
  function checkResponse(response, $route){
    if(response.data.status == 'success'){
      $route.reload()
    }
    notification(response.data.status, response.data.message, response.data.status)
  }
  