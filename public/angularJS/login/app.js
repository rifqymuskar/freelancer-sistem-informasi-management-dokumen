var app = angular.module("RifqyMuskar", ["ngRoute"])
app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/", {
      templateUrl : "auth/route/login",
      controller : "login"
  })
    .when("/login", {
        templateUrl : "auth/route/login",
        controller : "login"
    })
    .when("/register", {
        templateUrl : "auth/route/register",
        controller : "register"
    })
    .when("/forgotpassword", {
        templateUrl : "auth/route/forgotpassword",
        controller : "forgotpassword"
    })
    .when("/newpassword/:token", {
        templateUrl : "auth/route/newpassword",
        controller : "newpassword"
    })
    .otherwise({
        templateUrl : "auth/route/404",
        controller : "404"
    })

})

app.factory('request', function ($http) {
    return {
        get: function (url, params) {
            return $http({
                headers: {
                  'authentication': 'rifqymuskar'
                },
                method: 'GET',
                url: url,
                'cache': false,
                params : params
              }).then(function (response) { 
                return response
            })
        },
        post: function (url, data) {
            return $http({
                headers: {
                  'authentication': 'rifqymuskar'
                },
                method: 'POST',
                'cache': false,
                url: url,
                data : data
              }).then(function (response) { 
                return response
            })
        },
        put: function (url, data) {
            return $http({
                headers: {
                  'authentication': 'rifqymuskar'
                },
                method: 'PUT',
                'cache': false,
                url: url,
                data : data
              }).then(function (response) { 
                return response
            })
        },
        delete: function (url, data) {
            return $http({
                headers: {
                  'authentication': 'rifqymuskar'
                },
                method: 'DELETE',
                'cache': false,
                url: url
              }).then(function (response) { 
                return response
            })
        }
    };
})