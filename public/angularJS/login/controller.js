app.controller('login', function($scope, $http, request){
    $(function () {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      })
    })

    setTimeout(function(){ $(".username").focus() }, 500)

    $('.buttonSubmitLogin').on( 'keyup', function( e ) {
      if( e.which == 9 ) {
          $("#identity").focus() 
      }
  } );

    // AngularJS
    $scope.post = function(){
      check = request.post('api/checkUser/users', $scope.form)
      check.then(function (response) {
        if(response.data.status != 'success'){
          $scope.form = {}
          setTimeout(function(){ $(".username").focus() }, 500)
          notification(response.data.status, response.data.message, 'error')
        }else{
          location.reload()
        }
      })
    }

})

app.controller('forgotpassword', function($scope, $http, $location, request){
  $scope.post = function(){

    $(".loading").addClass("active")
    request.post("api/forgotPassword", $scope.form).then(function(response){
      $(".loading").removeClass("active")

      if(response.data.status == 'success'){
        $scope.form = {}
        $location.path("/login")
        swal({
          title: "Berhasil",
          text: "Berhasil melakukan reset, silahkan cek inbox email anda untuk mengganti password baru anda.",
          icon: "success",
          dangerMode: true,
          closeOnClickOutside: false
        })
      }else{
        $scope.form = {}
        swal({
          title: "Gagal",
          text: response.data.message,
          icon: "error",
          button: "Oke, Terimakasih",
        });
        setTimeout(function(){ $(".username").focus() }, 1000)
      }

    })
  }
})
app.controller('newpassword', function($scope, $http, $routeParams, $location, request){
  request.post("api/checkToken", $routeParams).then(function(response){

    $scope.dataUser = response.data.data

    if(response.data.status != 'success'){
      swal({
        title: "Token Salah",
        text: response.data.message,
        icon: "warning",
        dangerMode: true,
        closeOnClickOutside: false
      })
      .then((willDelete) => {
        if (willDelete) {
          window.location = "https://iwan.rifqymuskar.com/"
        }
      });
    }
  })
  $scope.post = function(){
    
    $scope.update = {
      "id" : $scope.dataUser.user_id,
      "password" : $scope.form.user,
      "token" : $routeParams.token
    }

    request.put("api/users", $scope.update).then(function(response){
      if(response.data.status == 'success'){
        swal({
          title: "Berhasil",
          text: "Password anda telah berhasil di update.",
          icon: "warning",
          dangerMode: true,
          closeOnClickOutside: false
        })
        .then((willDelete) => {
          if (willDelete) {
            window.location = "https://iwan.rifqymuskar.com/"
          }
        });
      }
    })
  }
})