function notification(heading, text, icon){
    $.toast({
        heading: heading,
        text: text,
        icon: icon,
        loader: true,        // Change it to false to disable loader
        showHideTransition: 'plain',
        hideAfter: 8000,
        loaderBg: '#9EC600'  // To change the background
    })
}