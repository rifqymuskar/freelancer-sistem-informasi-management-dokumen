var app = angular.module("RifqyMuskar", ["ngRoute", "ngAnimate", "ckeditor", "ngSanitize", "angularUtils.directives.dirPagination", "ui.select2", "thatisuday.dropzone", "chart.js", "wt.responsive", "ckeditor"])
var month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
var route = 'uploader/home/route/'
app.config(function($routeProvider, $locationProvider, dropzoneOpsProvider) {

    $routeProvider
    .when("/", {
      templateUrl : route+"dashboard",
      controller : "dashboard"
    })
    .when("/dashboard", {
        templateUrl : route+"dashboard",
        controller : "dashboard"
    })
    .when("/profile", {
        templateUrl : route+"profile",
        controller : "profile"
    })
    .when("/divisi", {
        templateUrl : route+"divisi",
        controller : "divisi"
    })
    .when("/divisi/new", {
        templateUrl : route+"divisi_new",
        controller : "divisi_new"
    })
    .when("/user", {
        templateUrl : route+"user",
        controller : "user"
    })
    .when("/user/new", {
        templateUrl : route+"user_new",
        controller : "user_new"
    })
    .when("/user/edit/:id", {
        templateUrl : route+"user_new",
        controller : "user_edit"
    })
    .when("/dokumen", {
        templateUrl : route+"dokumen",
        controller : "dokumen"
    })
    .when("/dokumen/new", {
        templateUrl : route+"dokumen_new",
        controller : "dokumen_new"
    })
    .when("/dokumen/edit/:id", {
      templateUrl : route+"dokumen_new",
      controller : "dokumen_edit"
    })
    .when("/perusahaan", {
        templateUrl : route+"perusahaan",
        controller : "perusahaan"
    })
    .when("/perusahaan/new", {
        templateUrl : route+"perusahaan_new",
        controller : "perusahaan_new"
    })

    .when("/pusat", {
        templateUrl : route+"pusat",
        controller : "pusat"
    })
    .when("/cabang", {
        templateUrl : route+"cabang",
        controller : "cabang"
    })
    
    .otherwise({
        templateUrl : "auth",
        controller : "notfound"
    })

    dropzoneOpsProvider.setOptions({
			dictDefaultMessage : 'Klik disini atau drag file anda',
			dictRemoveFile : 'Hapus file',
			dictResponseError : 'Could not upload this photo',
      maxFiles : 1
		});

})

app.factory('request', function ($http) {
    return {
        get: function (url, params) {
            return $http({
                headers: {
                  'authentication': 'rifqymuskar'
                },
                method: 'GET',
                url: url,
                'cache': false,
                params : params
              }).then(function (response) { 
                return response
            })
        },
        post: function (url, data) {
            return $http({
                headers: {
                  'authentication': 'rifqymuskar'
                },
                method: 'POST',
                'cache': false,
                url: url,
                data : data
              }).then(function (response) { 
                return response
            })
        },
        put: function (url, data) {
            return $http({
                headers: {
                  'authentication': 'rifqymuskar'
                },
                method: 'PUT',
                'cache': false,
                url: url,
                data : data
              }).then(function (response) { 
                return response
            })
        },
        delete: function (url, data) {
            return $http({
                headers: {
                  'authentication': 'rifqymuskar'
                },
                method: 'DELETE',
                'cache': false,
                url: url
              }).then(function (response) { 
                return response
            })
        }
    };
})

app.directive('file', function () {
  return {
      scope: {
          file: '='
      },
      link: function (scope, el, attrs) {
          el.bind('change', function (event) {
              var file = event.target.files[0];
              scope.file = file ? file : undefined;
              scope.$apply();
          });
      }
  };
});

app.run(function($rootScope, $location, $q, $timeout, request) {

  $rootScope.getClass = function (path) { 
    return ($location.path() == path) ? 'active' : ''
  }
  $rootScope.getClassParent = function (path) { 
    data = 0
    for(x in path){
      if(path[x] == $location.path()){
        data++
      }
    }
    if(data > 0){
      return 'active'
    }
  }

  $rootScope.getShow = function(path){
    data = 0
    for(x in path){
      if(path[x] == $location.path()){
        data++
      }
    }
    if(data > 0){
      return 'active menu-open'
    }
  }

  if($location.path() == "/"){
    $location.path("/dashboard")
  }

  if(typeof(EventSource) !== "undefined") {
    var source = new EventSource("Response/app_version");
    source.onmessage = function(event) {
  
      cookie = document.cookie
      var output = {};
      cookie.split(/\s*;\s*/).forEach(function(pair) {
        pair = pair.split(/\s*=\s*/);
        output[pair[0]] = pair.splice(1).join('=');
      });
      var json = JSON.stringify(output, null, 4);
      json = JSON.parse(json)
      if(json.version){
        $("#notification").html("<p>version application <b>"+json.version+"</b></p>")
        if(json.version != event.data){
          console.log(json.version)
          console.log(event.data)
          swal({
            title: "Informasi",
            text: "Developer Aplikasi Baru Saja Melakukan Update Aplikasi, Silahkan Klik Oke.",
            icon: "warning",
            // buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              date = new Date()
              date = new Date(date.setMonth(date.getMonth()+1))
              document.cookie = "version="+event.data+"; expires="+date
              location.reload(true);
            }
          })      
        }
      }else{
          date = new Date()
          date = new Date(date.setMonth(date.getMonth()+1))
          document.cookie = "version="+event.data+"; expires="+date
          location.reload(true);
      }
  
    };
  } else {
    $("#notification").text("terjadi kesalahan ketika mengambil data Event Source, hubungi Developer Aplikasi")
  }

  $rootScope.checkCookies = function(){
    request.get('api/session').then(function (response) {
      if(!response.data.uid){
        if(!response.data.id){
          window.location.replace("/");
        }
      }
    })
  }
  $rootScope.checkCookies()
  
  request.get('api/session').then(function (response) {
    $rootScope.session = response.data
  })

  $rootScope.getRole = function(){
    request.get('api/users_role').then(function (response) {
      $rootScope.dataRole = response.data.data
    })
  }
  $rootScope.getRole()

  $rootScope.getCabang = function(){
    request.get('api/users_company').then(function (response) {
      $rootScope.dataCabang = response.data.data
    })
  }
  $rootScope.getCabang()

  $rootScope.getDivisi = function(){
    request.get('api/users_department').then(function (response) {
      $rootScope.dataDivisi = response.data.data
    })
  }
  $rootScope.getDivisi()

  $rootScope.getPosisi = function(){
    request.get('api/users_position').then(function (response) {
      $rootScope.dataPosisi = response.data.data
    })
  }
  $rootScope.getPosisi()

  $rootScope.getAturan = function(){
    request.get('api/document_aturan').then(function (response) {
      $rootScope.dataAturan = response.data.data
    })
  }
  $rootScope.getAturan()

})

app.filter('limitHtml', function() {
  return function(text, limit) {

      var changedString = String(text).replace(/<[^>]+>/gm, '');
      var length = changedString.length;
      var suffix = ' ...'

      return length > limit ? changedString.substr(0, limit - 1) + suffix : changedString;
  }
});

app.filter('IDR', function() {
  return function(value) {
    if(parseInt(value) > 0){
      return parseInt(value).toLocaleString(['ban', 'id'])
    }else{
      return 0
    } 
  }
})
app.filter('phone', function() {
  return function(value) {
    if(parseInt(value) > 0){
      let joy=value.match(/.{1,4}/g)
      return joy.join(' ')
    }else{
      return 0
    } 
  }
})
app.filter('date', function(){
  return function(value){
    date = new Date(value)
    return date.getDate() + " " + month[date.getMonth()] + " " + date.getFullYear()
  }
})

app.directive('tooltip', function(){
  return {
      restrict: 'A',
      link: function(scope, element, attrs){
          element.hover(function(){
              // on mouseenter
              element.tooltip('show');
          }, function(){
              // on mouseleave
              element.tooltip('hide');
          });
      }
  };
});

app.directive('format', ['$filter', function ($filter) {
  return {
      require: '?ngModel',
      link: function (scope, elem, attrs, ctrl) {
          if (!ctrl) return;

          ctrl.$formatters.unshift(function (a) {
              return $filter(attrs.format)(ctrl.$modelValue)
          });

          elem.bind('keyup', function(event) {
              var plainNumber = elem.val().replace(/[^\d|\-+|\.+]/g, '');
              elem.val($filter(attrs.format)(plainNumber));
          });
      }
  };
}]);

app.controller('sidebar', function($scope, $location, request){
  $scope.logoutSession = function(){
    check = request.post('api/logout')
    check.then(function (response) {
      window.location.replace("")
    })
  }
})